#!/bin/bash  
tmin=0.50
tmax=300.00
nBins=5
nmodes=20 
dimless=Dimensionless_
# dimless=
MY_PATH=/Users/marika_asgary/Documents/CosmicShear/repos/
cfoldername=${MY_PATH}/dimless_cosebis/${dimless}data/
COSEBIs_PATH=${MY_PATH}/cosebis_cosmosis/
xipm_file_name_start=${MY_PATH}/Cat_to_Obs_K1000_P1/data/kids/xipm/XI_K1000_ALL_BLIND_C_V1.0.0A_ugriZYJHKs_photoz_SG_mask_LF_svn_309c_2Dbins_v2_goldclasses_Flag_SOM_Fid_nbins_4000_theta_0.5_300.0
for i in {1..5}
do
	for j in `seq ${i} 5`
	do
		bins_name=nBins_${nBins}_Bin${i}_Bin${j}
		xipm_file_name=${xipm_file_name_start}_${bins_name}.ascii
		echo ${bins_name}
		python run_measure_cosebis_cats2stats.py -i ${xipm_file_name} -o ${bins_name} --norm ${COSEBIs_PATH}/${dimless}TLogsRootsAndNorms/Normalization_${tmin}-${tmax}.table -r ${COSEBIs_PATH}/${dimless}TLogsRootsAndNorms/Root_${tmin}-${tmax}.table -b log --thetamin ${tmin} --thetamax ${tmax} -n ${nmodes} --tfoldername ${COSEBIs_PATH}/${dimless}Tplus_minus --cfoldername ${cfoldername} -t 0 -p 3 -m 4
	done
done
